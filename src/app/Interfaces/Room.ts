export interface Room{
  resourceName:string;
  resourceEmail:string;
  resourceId:string;
  features: [string];
  capacity: string;
  floorName: string;
  floorSection: string;
  userVisibleDescription: string;
  buildingName: string;
}
