export interface GoogleEvent{

  "id" : string;
  "summary": string;
  "description": string,
  "start": {
    "dateTime": string
  },
  "end": {
    "dateTime": string
  },
  "attendees": [string],
  "isRoomieEvent": boolean


}
