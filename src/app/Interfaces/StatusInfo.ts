export interface StatusInfo{
  "currentStatus": String,
  "startOfNextRoomieEvent": Date,
  "endOfNextRoomieEvent": Date,

}
