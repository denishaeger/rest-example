export interface AdminConfig{
  "startOfDay": number,
  "endOfDay": number,
  "defaultDuration": number,
  "bufferBeforeEvent": number,
  "bufferAfterEvent": number,
  "preBookingBuffer": number,
  "extendEventBy": number,
  "shortenEventBy": number,
  "defaultMeetingSummary": string,
  "customerId": string
}
