import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  Input,
} from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent {
  @Input()
  isDarkMode = false;

  @Output()
  readonly darkModeSwitched = new EventEmitter<boolean>();

  @Output()
  readonly apressed = new EventEmitter<String>();

  selected = 'option2';

  onDarkModeSwitched({ checked }: MatSlideToggleChange) {
    this.darkModeSwitched.emit(checked);
  }

  onDarkModeSwitched2({ checked }: MatSlideToggleChange) {
    let checkedd = "a"
    this.apressed.emit(this.selected);
  }
  onDarkModeSwitched3({ checked }: MatSlideToggleChange) {
    let checkedd = "b"
    this.apressed.emit(this.selected);
  }
}
