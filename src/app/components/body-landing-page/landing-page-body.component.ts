import { Component, ViewChildren } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Title} from "@angular/platform-browser";
import {ngAria} from 'angular-aria'
import { OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ActiveDescendantKeyManager, FocusMonitor, FocusOrigin, LiveAnnouncer} from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectorRef,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ENTER } from '@angular/cdk/keycodes';
import { QueryList } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { takeUntil } from 'rxjs/operators';
import { OverlayContainer } from '@angular/cdk/overlay';
import { ProductPickerComponent } from '../product-picker/product-picker.component';

@Component({
  selector: 'app-landing-page-body',
  templateUrl: './landing-page-body.component.html',
  styleUrls: ['./landing-page-body.component.scss']
})
export class LandingPageBodyComponent implements OnInit, OnDestroy, AfterViewInit {

  email = new FormControl('', [Validators.required, Validators.email]);


  private keyManager: ActiveDescendantKeyManager<ProductPickerComponent>;


selection = '';
a:string = "a";
isAltMode = false
tags = [{title:'Oben D',class:'fakeh1',role:'heading',level:'1'},{title:'Unten D',class:'fakeh2',role:'heading',level:'2'}]

@ViewChild('subtree') subtree: ElementRef<HTMLElement>;
@ViewChild('element') element: ElementRef<HTMLElement>;

// elementOrigin = this.formatOrigin(null);
// subtreeOrigin = this.formatOrigin(null);



constructor(
  @Inject(DOCUMENT) private document: Document,
  private renderer: Renderer2,
  private _focusMonitor: FocusMonitor,
  private _cdr: ChangeDetectorRef,
  private _ngZone: NgZone,
  private liveAnnouncer: LiveAnnouncer,
  private dialog: MatDialog,
  breakpointObserver: BreakpointObserver,private overlayContainer: OverlayContainer,
  private router: Router, private route: ActivatedRoute,
  private titleService:Title
) {
  // this.titleService.setTitle("Demo")
  // breakpointObserver
  //   .observe([
  //     Breakpoints.XSmall,
  //     Breakpoints.Small,
  //     Breakpoints.Medium,
  //     Breakpoints.Large,
  //     Breakpoints.XLarge,
  //   ])
  //   .pipe()
  //   .subscribe(result => {
  //     for (const query of Object.keys(result.breakpoints)) {
  //       if (result.breakpoints[query])
  //        {console.log(query);}
  //     }})


}

note:number = null
altTyp = false;
  get isDarkMode(): boolean {
    return this.currentTheme === 'theme-alt';
  }

  private currentTheme = 'theme-light';

  ngOnInit(): void {
    this.currentTheme = localStorage.getItem('activeTheme') || 'theme-light';
    this.renderer.setAttribute(this.document.body, 'class', this.currentTheme);

  }

  toggleTyp(){
this.altTyp=!this.altTyp
  }
  switchMode(isAltTheme: boolean) {
    this.isAltMode = !this.isAltMode
    this.currentTheme = this.isAltMode ? 'theme-alt' : 'theme-bad';
    this.renderer.setAttribute(this.document.body, 'class', this.currentTheme);
    // localStorage.setItem('activeTheme', this.currentTheme);
    // console.log(this.a);
    this.overlayContainer.getContainerElement().classList.add( this.currentTheme);
  }

  apressed(isAltTheme: string){
    console.log(isAltTheme);

    this.a = isAltTheme;
    if(this.a == 'd' ){
      // this.renderer.setAttribute(this.document.body, 'class', 'theme-bad');
    }
    else{
      this.renderer.setAttribute(this.document.body, 'class', 'theme-light');
    }

    if(this.a == 'e'){
      // setTimeout(()=>{
        // this._focusMonitor.monitor(this.subtree, true).subscribe(origin =>
        //   this._ngZone.run(() => {
        //     this.subtreeOrigin = this.formatOrigin(origin);
        //     this._cdr.markForCheck();
          // }),
        // );   }, 100);
    }
  }
/** @title Monitoring focus with FocusMonitor */


  onKeyUp(event) {

  }

  ngAfterViewInit() {
    this.liveAnnouncer.announce("Hello Screenreader User");
  }

  ngOnDestroy() {

  }

  // formatOrigin(origin: FocusOrigin): string {
  //   return origin ? origin + ' focused' : 'blurred';
  // }

  openModal() {
    this.router.navigate(['/Info'], { relativeTo: this.route });
}
openModal2() {
  this.dialog.open(ProductPickerComponent).afterClosed().subscribe(ret => {
    this.note = 1;
  })
}

register() {
  // this._focusMonitor.monitor(this.subtree, true).subscribe(origin =>
  //   this._ngZone.run(() => {
  //     this.subtreeOrigin = this.formatOrigin(origin);
  //     this._cdr.markForCheck();
  //   }),
  // );
}


getErrorMessage() {
  if (this.email.hasError('required')) {
    return 'You must enter a value';
  }

  return this.email.hasError('email') ? 'Not a valid email' : '';
}

}
