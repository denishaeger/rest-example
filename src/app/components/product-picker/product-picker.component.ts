/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

export interface ColorDialogData {
  color: string;
}

@Component({
  selector: 'app-product-picker',
  templateUrl: './product-picker.component.html',
  styleUrls: ['./product-picker.component.scss']
})
export class ProductPickerComponent implements OnInit {

  public products: string[] = [
    'Ball',
    'Blume',
    'Farbe',
    'Fahrrad',
    'Sofa',
  ];

  // TODO: #11. Announce changes with LiveAnnouncer
  constructor(public dialogRef: MatDialogRef<ProductPickerComponent>,
    private liveAnnouncer: LiveAnnouncer) { }

  ngOnInit(): void { }

  public addProduct(product: string): void {    
    if (product) {
      this.liveAnnouncer.announce(`Produkt ${product} hinzugefügt`);
    }      setTimeout(()=>{
      this.dialogRef.close();
    }, 100);    
  }

  public closeDialog(): void {
    this.dialogRef.close();
  }
}
