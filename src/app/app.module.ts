import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./material/material.module";
import {
  CalendarDateFormatter,
  CalendarModule,
  CalendarNativeDateFormatter,
  DateAdapter,
  DateFormatterParams
} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {Routes, RouterModule} from "@angular/router";
import { LandingPageBodyComponent } from './components/body-landing-page/landing-page-body.component';
import {MatOptionModule} from "@angular/material/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {
  GoogleApiModule,
  GoogleApiService,
  GoogleAuthService,
  NgGapiClientConfig,
  NG_GAPI_CONFIG,
  GoogleApiConfig
} from "ng-gapi";
import {UserService} from "./services/user.service";
import {CommonModule} from "@angular/common";
import {NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import {FlatpickrModule} from "angularx-flatpickr";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { LayoutModule } from '@angular/cdk/layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { NavigationComponent } from './components/navigation/navigation.component';
import {MatListModule} from '@angular/material/list';
import { A11yModule } from '@angular/cdk/a11y';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ProductPickerComponent } from './components/product-picker/product-picker.component';
import {MatBadgeModule} from '@angular/material/badge';
import { Page1Component } from './components/page1/page1.component';
import { Page2Component } from './components/page2/page2.component';




// let gapiClientConfig: NgGapiClientConfig = {
//   client_id: "458429880644-k0efdgbgme3dp0jn4c98q5d9o9d6ti66.apps.googleusercontent.com",
//   discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
//   scope: [
//     "https://www.googleapis.com/auth/calendar",
//     "https://apps-apis.google.com/a/feeds/calendar/resource/"
//   ].join(" ")
// };

class CustomDateFormatter extends CalendarNativeDateFormatter {

  public dayViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat('ca', {
      hour: 'numeric',
      minute: 'numeric'
    }).format(date);
  }

}

const appRoutes: Routes = [
  { path: '', component: LandingPageBodyComponent, data: { title: 'Demo' } },
  { path: '**', component: LandingPageBodyComponent },
];

@NgModule({
  exports:[
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
  ],
  declarations: [
    ProductPickerComponent,
    AppComponent,
    LandingPageBodyComponent,
    NavigationComponent,
    Page1Component,
    Page2Component,

  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatBadgeModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule,
    MatInputModule,
    MatTableModule,
    A11yModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    DragDropModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    },{dateFormatter: {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
      }}),
    MatOptionModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
