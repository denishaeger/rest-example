import { Component, ViewChildren } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Title} from "@angular/platform-browser";
import {ngAria} from 'angular-aria'
import { OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {ActiveDescendantKeyManager, FocusMonitor, FocusOrigin, LiveAnnouncer} from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectorRef,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ENTER } from '@angular/cdk/keycodes';
import { QueryList } from '@angular/core';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {



selection = '';

  numbers: any[];

  get isDarkMode(): boolean {
    return this.currentTheme === 'theme-alt';
  }

  private currentTheme = 'theme-light';

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => {
          let route: ActivatedRoute = this.router.routerState.root;
          let routeTitle = '';
          while (route!.firstChild) {
            route = route.firstChild;
          }
          if (route.snapshot.data['title']) {
            routeTitle = route!.snapshot.data['title'];
          }
          return routeTitle;
        })
      )
      .subscribe((title: string) => {
        if (title) {
          this.titleService.setTitle(`${title}`);
        }
      });
  }

  switchMode(isAltTheme: boolean) {
    this.currentTheme = isAltTheme ? 'theme-alt' : 'theme-light';
    this.renderer.setAttribute(this.document.body, 'class', this.currentTheme);
    localStorage.setItem('activeTheme', this.currentTheme);
  }


/** @title Monitoring focus with FocusMonitor */

  @ViewChild('element') button: ElementRef<HTMLElement>;
  @ViewChild('subtree') element: ElementRef<HTMLElement>;

  elementOrigin = this.formatOrigin(null);
  subtreeOrigin = this.formatOrigin(null);

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private _focusMonitor: FocusMonitor,
    private _cdr: ChangeDetectorRef,
    private _ngZone: NgZone,
    private liveAnnouncer: LiveAnnouncer,
    private dialog: MatDialog,
    private router: Router,
    private titleService: Title
  ) {
  }

  onKeyUp(event) {

  }

  ngAfterViewInit() {
    this.liveAnnouncer.announce("Hello Screenreader User");
    // this._focusMonitor.monitor(this.button).subscribe(origin =>
    //   this._ngZone.run(() => {
    //     this.elementOrigin = this.formatOrigin(origin);
    //     // this._cdr.markForCheck();
    //   }),
    // );
    // this._focusMonitor.monitor(this.element, true).subscribe(origin => {
    //         console.log(origin);
    // })

  }

  ngOnDestroy() {
    // this._focusMonitor.stopMonitoring(this.button);
    // this._focusMonitor.stopMonitoring(this.element);
  }

  formatOrigin(origin: FocusOrigin): string {
    return origin ? origin + ' focused' : 'blurred';
  }

  openModal() {
}
openModal2() {
}
}




// copyrouter.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe(() => {
//   const mainHeader = document.querySelector('#main-content-header')
//   if (mainHeader) {
//     mainHeader.focus();
//   }
// });


