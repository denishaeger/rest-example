import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {Room} from "../Interfaces/Room";
import {Roomfeatures} from "../Interfaces/Roomfeatures";
import {catchError} from "rxjs/operators";
import {GoogleEvent} from "../Interfaces/GoogleEvent";
import {AdminConfig} from "../Interfaces/AdminConfig";
import {addHours, addMinutes} from "date-fns";
import { UserService } from './user.service';



@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  httpOptions = {
    headers: new HttpHeaders({'observe': 'response', 'access_token': this.userService.getToken()})
  }

  private apiURL = 'http://localhost:5000/apirooms';
  private URL = 'http://localhost:3000/'
  //private URL = 'https://roomie2021.hs-augsburg.de:3000/'
  // private URL = 'https://my-backend-xegpaapsbq-uc.a.run.app/'

  constructor(private http: HttpClient, private userService: UserService) { }

//https://confluence.informatik.hs-augsburg.de/display/SR/Backend-Frontend-Komunikation api doku

  getRooms():Observable<Room[]> {
    return this.http.get<Room[]>(this.apiURL);//catcherror => POPUP "adimn muss sich anmelden" an PC ODER weiterleitung an admin seite
  }
  // getRooms(): Observable<Room[]> {
  //   return this.http.get<Room[]>(this.URL + "api/rooms", this.userService.getHeader());//catcherror => POPUP "adimn muss sich anmelden" an PC ODER weiterleitung an admin seite
  // }



  // sendAccessToken(accessToken: string): Observable<Object>{
  //   console.log(accessToken)
  //   const accesstokenJ:JSON = <JSON><unknown>{
  //     "token": accessToken
  //   }
  //   return this.http.post('http://localhost:5000/apisettingsaccesstoken', accessToken);
  // }
  sendAccessToken(accessToken: string): Observable<Object>{
    const accesstokenJ:JSON = <JSON><unknown>{
      "accesstoken": accessToken
    }
    return this.http.post(this.URL + "api/settings/accesstoken/", accesstokenJ);
  }

    getEvents(roomEmail: string): Observable<GoogleEvent[]>{
      return this.http.get<GoogleEvent[]>("http://localhost:5000/apieventsRaum02@resource.google.com")
    }
  // getEvents(roomEmail: string): Observable<GoogleEvent[]>{
  //   return this.http.get<GoogleEvent[]>(this.URL + "api/events/" + roomEmail, this.userService.getHeader());
  // }

//  createEvent(start + endzeitpunkt + titel):{
  createEvent(roomEmail: string, startOfNextRoomieEvent: Date, endOfNextRoomieEvent: Date): Observable<Object>{
      const jsonEvent:JSON = <JSON><unknown>{
        "summary": "Tür Buchung",
        "description": "Tür Buchung",
        "start": {
          "dateTime": startOfNextRoomieEvent
        },
        "end": {
          "dateTime": endOfNextRoomieEvent
        },
        "attendees": [
          ""
        ]
      }
    return this.http.post('http://localhost:5000/apieventsRaum02@resource.google.com', jsonEvent);
    // return this.http.post<GoogleEvent>(this.URL + "api/events/" + roomEmail, jsonEvent, this.userService.getHeader());
  }


  editEvents(roomEmail: string, id: string, duration: number, dateTime: string): Observable<GoogleEvent[]>{
    const editsJ:JSON = <JSON><unknown>{
      duration: duration,

      end: {

        "dateTime": dateTime

      }
    }
    return this.http.patch<GoogleEvent[]>(this.URL + "api/events/" + roomEmail + "/" + id, editsJ, this.userService.getHeader());
  }

  deleteEvents(roomEmail: string, id: string): Observable<GoogleEvent[]>{
    return this.http.delete<GoogleEvent[]>(this.URL + "api/events/" + roomEmail + "/" + id, this.userService.getHeader());
  }

  // getConfig(parameter: string): Observable<AdminConfig>{
  //   return this.http.get<AdminConfig>(this.URL + "apisettings" + parameter)
  // }
  getConfig(parameter: string): Observable<AdminConfig>{
    return this.http.get<AdminConfig>(this.URL + "api/settings/" + parameter)
  }

  //sendCustomerID(cID)

  //sendVerificationRequest(password) authentifiziern hält bis seite gewechselt wird ?
  sendVerificationRequest(password: string): Observable<Object>{
    let body = {
      pwd: password
    }
    return this.http.post(this.URL + 'api/settings/verify', body);
  }
// sendVerificationRequest(password: string):Observable<Object>{
//
//     return this.http.post(this.URL + "api/settings/verify", password, {observe:"response"});
//   }

  //configSetting(änderungen ???)
  sendConfigUpdate(config: AdminConfig) {
    return this.http.post<GoogleEvent>(this.URL+'api/settings', config, this.userService.getHeader());
  }
 // sendConfigUpdate(config: AdminConfig) {
 //    return this.http.post<GoogleEvent>("http://localhost:5000/apisettingsaccesstoken", config, httpOptions);
 //  }

  private customSubject = new Subject<any>();
  customObservable = this.customSubject.asObservable();

  // Service message commands
  callComponentMethod() {
    this.customSubject.next();
  }

}
