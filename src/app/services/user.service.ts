import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService {

  public isAuthorized: boolean = false; // Ist der User eingeloggt?
  public isLoaded: boolean = false;     // sind die Gapis und auth fertig mit laden?
  private isLoadedObs: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);  // Hiermit benachrichten jeden der es wissen will
  public isLoadedVar = this.isLoadedObs.asObservable(); // der tatsächliche observer
  private GoogleAuth: gapi.auth2.GoogleAuth = undefined; // Das objekt das uns gapi zurück gibt. Wieso groß? vgl Docs & kommt einer Klasse gleich was weiß ich

  constructor(private ngZone: NgZone) {
    try {
      gapi.load('client:auth2', () => {
      console.log("Auth2 geladen....");
      this.initGapi();
    });
    } catch (error) {
      console.error(error);
    }
  }

  // Darf erst ausgeführt werden, wenn gapis geladen sind (siehe constructor)
  private initGapi() {
    gapi.client.init({
      'clientId': '458429880644-2vlq36qhodibj7jqrck6gq8s046roio1.apps.googleusercontent.com',
      'scope': 'https://www.googleapis.com/auth/calendar https://apps-apis.google.com/a/feeds/calendar/resource/',
    }).then(() => {
      console.log("Gapi geladen....");

      this.GoogleAuth = gapi.auth2.getAuthInstance();
      console.log(this.GoogleAuth);

      this.updateLoginState(this.GoogleAuth.isSignedIn["fe"]);

      this.GoogleAuth.isSignedIn.listen(isSignedIn => {
        this.updateLoginState(isSignedIn);
      });
      this.GoogleAuth.currentUser.listen(data => {  //nur zu debugging zwecken
        console.log("Currently user:");
        console.log(data);
      })

      this.updateLoadedState(true); // erst wenn alles abgeschlossen ist
    });
  }

  // ngZone damit html elemente "benachrichtigt" werden (login button etc)
  private updateLoginState(state) {
    console.log(`User is now ${state ? "logged in" : "logged out"}`);
    this.ngZone.run(() => this.isAuthorized = state);
  }

  // ngZone damit html elemente "benachrichtigt" werden (divs in diverse Seiten)
  private updateLoadedState(state) {
    this.ngZone.run(() => this.isLoaded = state);
    this.isLoadedObs.next(state);
  }

  public signIn(): void {
    this.GoogleAuth.signIn();
  }

  public signOut(): void {
    this.GoogleAuth.signOut();
  }

  public getToken(): string {
    if (this.isAuthorized) {
      return this.GoogleAuth.currentUser.get().getAuthResponse().access_token;
    } else {
      return null;
    }
  }

  public getUserName() {
    if (this.isAuthorized) {
      return this.GoogleAuth.currentUser.get().getBasicProfile().getGivenName();
    } else {
      return null;
    }
  }

  public getUserEmail() {
    if (this.isAuthorized) {
      return this.GoogleAuth.currentUser.get().getBasicProfile().getEmail();
    } else {
      return null;
    }
  }

  public getHeader() {
    return { "headers": { "access_token": this.getToken() } };
  }
}
